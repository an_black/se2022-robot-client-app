/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 *
 * @Author Anastasiia Purtova
 *
 * @Date 21.05.2022
 *
 * This is a modified React native template for educative purpose only.
 */

import React from 'react';
import {WelcomeView} from './app/views/WelcomeView';
import {OverviewView} from './app/views/OverviewView';

const App = () => {
  const [showUseCases, setShowUseCases] = React.useState<boolean>(false);

  const handleShowUseCase = React.useCallback(() => setShowUseCases(true), []);
  const handleHideUseCase = React.useCallback(() => setShowUseCases(false), []);

  return (
    <>
      {showUseCases ? (
        <OverviewView handleHideUseCase={handleHideUseCase} />
      ) : (
        <WelcomeView handleShowUseCase={handleShowUseCase} />
      )}
    </>
  );
};

export default App;
