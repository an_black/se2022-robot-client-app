import {SafeAreaView, StatusBar, StyleSheet, Text, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Section} from '../ui-components/Section';
import {Button} from 'react-native-paper';
import React from 'react';

export const WelcomeView: React.FC<{handleShowUseCase: () => void}> = ({
  handleShowUseCase,
}) => {
  const backgroundStyle = {
    backgroundColor: Colors.lighter,
  };
  return (
    <SafeAreaView style={backgroundStyle}>
      <View>
        <StatusBar barStyle={'dark-content'} />
        <Text style={styles.header}>
          Welcome to the Cleaning Robot Application
        </Text>
      </View>

      <View
        style={{
          backgroundColor: Colors.white,
        }}>
        <Section title="Introduction">
          <Text>
            This is the test screen for demonstration purpose only. You can
            navigate from here to see the use cases that were covered within
            this task scope.
          </Text>
          <Button mode="outlined" onPress={handleShowUseCase}>
            Go to use cases
          </Button>
        </Section>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  header: {
    fontSize: 18,
    margin: 50,
    fontWeight: '400',
    textAlign: 'center',
  },
  button: {
    marginTop: 20,
  },
  headerBlock: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  statusText: {
    marginLeft: 20,
    marginTop: 40,
  },
  avatar: {
    marginRight: 20,
    marginTop: 40,
    marginLeft: 'auto',
  },
  cardButtonSecond: {
    backgroundColor: 'orange',
  },
});
