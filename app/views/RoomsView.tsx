import {Button, Card, Headline, Title} from 'react-native-paper';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {getRoomType, Room, RoomType} from '../models/RoomModel';

export const RoomsView: React.FC<{
  rooms: Array<Room>;
}> = ({rooms}) => {
  const getPicture = (room: RoomType) => {
    switch (room) {
      case 'LIVING_ROOM':
        return require('../../images/livingRoom.jpg');
      case 'DINING_ROOM':
        return require('../../images/diningRoom.jpg');
      case 'HALL':
        return require('../../images/hall.jpg');
    }
  };
  return (
    <View style={styles.sectionContainer}>
      <Headline>Overview of the regions</Headline>
      <Text style={{display: 'flex', marginBottom: 10}}>Available regions</Text>
      {rooms.map((room, index) => (
        <Card key={index} style={styles.card}>
          <ImageBackground
            source={getPicture(getRoomType(room.name))}
            resizeMode="cover"
            style={styles.image}>
            <Card.Content>
              <Title style={styles.cardTitle}>{room.name}</Title>
              <Button
                mode="contained"
                onPress={() => console.log('Show Map')}
                style={[styles.cardButton, styles.cardButtonFirst]}>
                Show Map
              </Button>
              <Text> </Text>
              <Button
                mode="contained"
                onPress={() => console.log('Start Cleaning')}
                style={[styles.cardButton, styles.cardButtonSecond]}>
                <Text style={{color: 'black'}}>Start Cleaning</Text>
              </Button>
            </Card.Content>
          </ImageBackground>
        </Card>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
  },
  listOfRoomsBlock: {
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    width: '100%',
    minHeight: 250,
    marginBottom: 20,
    borderStyle: 'solid',
    borderColor: 'black',
  },
  cardTitle: {
    marginLeft: 'auto',
    marginRight: 10,
    marginBottom: 40,
    color: '#C9F5F8',
  },
  cardButton: {
    width: '50%',
    marginTop: 0,
    marginBottom: 0,
    marginRight: 'auto',
    marginLeft: 'auto',
    borderRadius: 50,
  },
  cardButtonFirst: {
    backgroundColor: '#8BDADF',
  },
  cardButtonSecond: {
    backgroundColor: '#E5FDFF',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    opacity: 0.8,
  },
});
