import React from 'react';
import {ScrollView, StatusBar, Text, View} from 'react-native';
import {Button} from 'react-native-paper';
import {RoomsView} from './RoomsView';
import {ModesDropdownView} from './ModesDropdownView';
import {Room} from '../models/RoomModel';
import {Mode} from '../models/ModeModel';
import {RobotController} from '../controllers/RobotController';
import {Header} from '../ui-components/Header';

export const OverviewView: React.FC<{handleHideUseCase: () => void}> = ({
  handleHideUseCase,
}) => {
  const [rooms, setRooms] = React.useState<Array<Room>>([]);
  const [modes, setModes] = React.useState<Array<Mode>>([]);
  const [status, setStatus] = React.useState<string>();

  const robotController = React.useMemo(() => RobotController(), []);

  const roomsFetcher = React.useMemo(
    () => robotController.getRooms('rooms'),
    [robotController],
  );

  const modesFetcher = React.useMemo(
    () => robotController.getCleaningModes('modes'),
    [robotController],
  );

  const statusFetcher = React.useMemo(
    () => robotController.getStatus('status'),
    [robotController],
  );

  React.useEffect(() => {
    roomsFetcher
      .then(response => response.json())
      .then(json => setRooms(json))
      .catch(error => console.error(error));
  }, [robotController, roomsFetcher]);

  React.useEffect(() => {
    modesFetcher
      .then(response => response.json())
      .then(json => setModes(json))
      .catch(error => console.error(error));
  }, [modesFetcher, robotController]);

  React.useEffect(() => {
    statusFetcher
      .then(response => response.text())
      .then(text => setStatus(text))
      .catch(e => console.log(e));
  }, [statusFetcher]);

  return (
    <>
      <StatusBar barStyle={'dark-content'} />
      <ScrollView>
        <View>
          <Header status={status} />

          <Text />

          {rooms.length ? <RoomsView rooms={rooms} /> : null}

          {modes.length ? <ModesDropdownView cleaningModes={modes} /> : null}

          <Button mode="outlined" onPress={handleHideUseCase}>
            Go back
          </Button>
        </View>
      </ScrollView>
    </>
  );
};
