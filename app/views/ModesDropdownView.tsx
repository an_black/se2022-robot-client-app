import React from 'react';
import {Text, View} from 'react-native';
import {Button, Divider, Modal, Portal, Provider} from 'react-native-paper';
import {Mode} from '../models/ModeModel';

export const ModesDropdownView: React.FC<{
  cleaningModes: Array<Mode>;
}> = ({cleaningModes}) => {
  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);

  const hideModal = () => setVisible(false);

  const containerStyle = {backgroundColor: 'white', padding: 20};

  return (
    <Provider>
      <Portal>
        <Modal
          visible={visible}
          onDismiss={hideModal}
          contentContainerStyle={containerStyle}>
          <View>
            {cleaningModes.length &&
              cleaningModes.map((mode, index) => (
                <>
                  <Button onPress={() => {}} key={index}>
                    {mode.name}
                  </Button>
                  <Divider />
                </>
              ))}
          </View>
        </Modal>
      </Portal>
      <Button mode="contained" style={{marginTop: 30}} onPress={showModal}>
        Show Cleaning Modes
      </Button>
      <Text> </Text>
    </Provider>
  );
};
