export const RobotController = () => {
  const url = 'http://0.0.0.0:8080/';

  const getRooms = (path: string) => fetch(url + path);

  const getCleaningModes = (path: string) => fetch(url + path);

  const getStatus = (path: string) => fetch(url + path);

  return {
    getRooms,
    getCleaningModes,
    getStatus,
  };
};
