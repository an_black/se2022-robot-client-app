import {StyleSheet, Text, View} from 'react-native';
import {Avatar} from 'react-native-paper';
import React from 'react';

export const Header: React.FC<{status?: string}> = ({status}) => {
  return (
    <View style={styles.headerBlock}>
      <Text style={styles.statusText}>
        {'Robot '}
        {status && status}
      </Text>
      <Avatar.Text size={48} label={'AP'} style={styles.avatar} />
    </View>
  );
};

const styles = StyleSheet.create({
  headerBlock: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  statusText: {
    marginLeft: 20,
    marginTop: 40,
  },
  avatar: {
    marginRight: 20,
    marginTop: 40,
    marginLeft: 'auto',
  },
});
