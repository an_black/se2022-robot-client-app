export type Room = {
  name: string;
};

export type RoomType = 'LIVING_ROOM' | 'DINING_ROOM' | 'HALL';

export const getRoomType = (roomName: string): RoomType => {
  if (roomName === 'Dining Room') {
    return 'DINING_ROOM';
  }
  if (roomName === 'Living Room') {
    return 'LIVING_ROOM';
  }
  if (roomName === 'Hall Room') {
    return 'HALL';
  }
  return 'DINING_ROOM';
};
